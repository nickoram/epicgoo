﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SFXCheckmark : MonoBehaviour {
    
    void HandleSFXEnabledChanged(bool isChecked) {
        AudioSingleton audioSingleton = AudioSingleton.instance;
        audioSingleton.SetSFXMuted(!isChecked);
    }
    
    // Use this for initialization
    void Start () {
        GUIScaler.Initialize();
        GUIScaler.Begin();
        Toggle sfxToggle = GetComponent<Toggle>();

        sfxToggle.isOn = !AudioSingleton.instance.IsSFXMuted();
        
        sfxToggle.onValueChanged.AddListener(HandleSFXEnabledChanged);
        
        GUIScaler.End();
    }
    
    // Update is called once per frame
    void Update () {
        
    }
}
