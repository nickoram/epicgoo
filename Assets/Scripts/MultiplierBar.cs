﻿using UnityEngine;
using System.Collections;

public class MultiplierBar : MonoBehaviour {

	public float barDisplay; //current progress

    public int positionScoreX = 25;
    public int positionScoreY = 40;
    public int dimensionsScoreX = 300;
    public int dimensionsScoreY = 60;

    private int positionMultiplierBarX = 20;
    private int positionMultiplierBarY = 87;
    private int dimensionsMultiplierBarX = 300;
    private int dimensionsMultiplierBarY = 30;

	public Texture2D emptyTexure;
	public Texture2D fullTexture;
	public Texture2D fontTexture;

	private float startTime;
	private const int secondsPerMultiplier = 5;

	private int multiplier;
    private int totalScore;

	void Start() {
		multiplier = 1;
        totalScore = 0;

        GUIScaler.Initialize();
	}

    public void resetMultiplier() {
        multiplier = 1;
    }

	public void multiplierHit() {
		multiplier++;
		startTime = Time.time + secondsPerMultiplier;
	}

	public int getMultiplier() {
		return multiplier;
	}

    public void setScore(int score) {
        totalScore = score;
    }
	
	void OnGUI() {	
        GUIScaler.Begin();
        // Draw total score
        GUI.Label(new Rect(positionScoreX, positionScoreY, dimensionsScoreX, dimensionsScoreY), "<color=\"#215691\"><size=35>" + totalScore + " POINTS</size></color>");
        GUIScaler.End();

        if (multiplier == 1 || getSecondsRemaining() <= 0) {
			return;
		}

        GUIScaler.Begin();
		//draw the background:
		GUI.BeginGroup(new Rect(positionMultiplierBarX, positionMultiplierBarY, dimensionsMultiplierBarX, dimensionsMultiplierBarY));
		GUI.Box(new Rect(0,0, dimensionsMultiplierBarX, dimensionsMultiplierBarY), emptyTexure);

		//draw the filled-in part:
		GUI.BeginGroup(new Rect(0,0, dimensionsMultiplierBarX * barDisplay, dimensionsMultiplierBarY));
		GUI.Box(new Rect(0,0, dimensionsMultiplierBarX, dimensionsMultiplierBarY), fullTexture);
		GUI.EndGroup();

		// Draw the text
		GUI.Box(new Rect(0,0, dimensionsMultiplierBarX, dimensionsMultiplierBarY), "<size=18>" + multiplier + "x</size>");
		GUI.EndGroup();
        GUIScaler.End();
	}
	
	void Update() {
		float secondsRemaining = getSecondsRemaining();
		if (secondsRemaining <= 0f) {
			multiplier = 1;
			return;
		}

		barDisplay = secondsRemaining / secondsPerMultiplier;
	}
	
	public float getSecondsRemaining() {
		return startTime - Time.time;
	}
}