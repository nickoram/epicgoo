﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour {

	void Start() {
		var manager = GameManager.active;
		var canvas = GameObject.Find ("Canvas");
			
		Text[] board = canvas.GetComponentsInChildren<Text>();

		foreach (var element in board) {
			switch (element.name) {
				case "TopScoreText":
					element.text = manager.TopScore.ToString();
					break;
				case "TopTimeText":
					element.text = manager.TopTime.ToString() + "s";
					break;
				case "ScoreText":
					element.text = manager.LastScore.ToString();
					break;
				case "TimeText":
					element.text = manager.LastTime.ToString() + "s";
					break;
			}
		}
	}

    void Update () {
        if (Input.anyKey) {
            Application.LoadLevel("level_01");
        }
    }

	public void OnClick() {
		Application.LoadLevel("level_01");
	}

}
