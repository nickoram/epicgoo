using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PauseMenu : MonoBehaviour {

    void GoToMainMenu() {
        Application.LoadLevel("title_screen");
    }

    void RestartLevel() {
        Application.LoadLevel(Application.loadedLevel);    
    }

    void Start() {
        GUIScaler.Initialize();
        GUIScaler.Begin();

        Button restartButton = GameObject.Find("Restart Level Button").GetComponent<Button>();
        restartButton.onClick.AddListener(RestartLevel);

        Button mainMenuButton = GameObject.Find("Main Menu Button").GetComponent<Button>();
        mainMenuButton.onClick.AddListener(GoToMainMenu);

        GUIScaler.End();
    }

    void Update() {
    }
}
