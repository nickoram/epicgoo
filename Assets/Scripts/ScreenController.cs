﻿using UnityEngine;
using System.Collections;

public class ScreenController : MonoBehaviour {
    public GameObject logo;

    private float smooth = 2.0F;
    private float tiltAngle = 45.0F;

    void Start() {
        logo = GameObject.Find("titlescreen_logo");
    }

	void Update () {
		if (Input.anyKey) {
			Application.LoadLevel("level_01");
		}

        float tiltAroundZ = Input.acceleration.x * tiltAngle;
        float tiltAroundX = -Input.acceleration.y * tiltAngle;
        Quaternion target = Quaternion.Euler(tiltAroundX, 0, tiltAroundZ);
        logo.transform.rotation = Quaternion.Slerp(logo.transform.rotation, target, Time.deltaTime * smooth);
	}
}
