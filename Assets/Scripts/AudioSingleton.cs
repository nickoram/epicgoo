﻿using UnityEngine;
using System.Collections;

public class AudioSingleton : MonoBehaviour {
    private static AudioSingleton _instance;

    private AudioClip backgroundMusic;
    private bool sfxEnabled;
    
    public static AudioSingleton instance {
        get {
            if (_instance == null) {
                _instance = GameObject.FindObjectOfType<AudioSingleton>();
                
                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }
            
            return _instance;
        }
    }
    
    void Awake() {
        if (!instance.audio.isPlaying) {
            instance.audio.Play();
        }
    }

    public void SetMute(bool mute) {
        instance.audio.mute = mute;
    }

    public void SetSFXMuted(bool mute) {
        sfxEnabled = mute;
    }

    public bool IsSFXMuted() {
        return sfxEnabled;
    }

    public bool IsMuted() {
        return instance.audio.mute;
    }

    // Use this for initialization
    void Start() {
    }
    
    // Update is called once per frame
    void Update() {
    }
}
