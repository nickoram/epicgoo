﻿using UnityEngine;
using System.Collections;

public class ExitLevel : MonoBehaviour {

    void OnCollisionEnter2D(Collision2D collision){
        Debug.Log("something has hit me");
        var level = GameObject.Find("LevelController");
        level.GetComponent<LevelController>().LevelComplete(true);
    }  
}
