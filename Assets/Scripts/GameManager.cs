// http://rusticode.com/2013/12/11/creating-game-manager-using-state-machine-and-singleton-pattern-in-unity3d/
using UnityEngine;

public class GameManager : MonoBehaviour {
	
	private static GameManager _manager;

	public static GameManager active { get { return _manager ?? (_manager = new GameManager()); } }
	
	public int TopScore { 
		get;
		private set;
	}

	public int TopTime {
		get;
		private set;
	}
	
	public int LastScore {
		get;
		private set;

	}

	public int LastTime {
		get;
		private set;
	}	

	private void LoadScores() {
		TopScore = PlayerPrefs.GetInt ("topScore");
		TopTime = PlayerPrefs.GetInt ("topTime");
		LastScore = PlayerPrefs.GetInt ("lastScore");
		LastTime = PlayerPrefs.GetInt ("lastTime");
	}

	private void SaveScores() {
		PlayerPrefs.SetInt ("topScore", TopScore);
		PlayerPrefs.SetInt ("topTime", TopTime);
		PlayerPrefs.SetInt ("lastScore", LastScore);
		PlayerPrefs.SetInt ("lastTime", LastTime);
	}

	public void UpdateScores(int time, int points) {
		LoadScores();

		if (points >= TopScore) {
			TopScore = points;
		}
		if (time < TopTime || TopTime == 0) {
			TopTime = time;
		}

		LastScore = points; 
		LastTime = time;

		SaveScores();
	}
}
