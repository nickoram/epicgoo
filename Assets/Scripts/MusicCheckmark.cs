﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MusicCheckmark : MonoBehaviour {

    void HandleMusicEnabledChanged(bool isChecked) {
        AudioSingleton audioSingleton = AudioSingleton.instance;
        audioSingleton.SetMute(!isChecked);
    }

	// Use this for initialization
	void Start () {
        GUIScaler.Initialize();
        GUIScaler.Begin();
        Toggle musicToggle = GetComponent<Toggle>();


        musicToggle.isOn = !AudioSingleton.instance.IsMuted();

        musicToggle.onValueChanged.AddListener(HandleMusicEnabledChanged);

        GUIScaler.End();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
